//
//  ImageCaptureViewController.swift
//  shishi-ios
//
//  Created by Lingyu Wang on 2018-05-13.
//  Copyright © 2018 ShiShi. All rights reserved.
//

import UIKit
import RealmSwift
import ShiShiSdk

class ImageCaptureViewController: UIViewController {
    
    var image:UIImage?
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        
    }
    
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = image
        
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
}

