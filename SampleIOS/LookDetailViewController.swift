//
//  ProductDetailViewController.swift
//  SampleIOS
//
//  Created by Lingyu Wang on 2018-05-13.
//  Copyright © 2018 ShiShi. All rights reserved.
//

import UIKit
import RealmSwift
import AlamofireImage
import ShiShiSdk

class LookDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, LookInfoViewProtocol {

    
    @IBAction func closeButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func tryOnButtonClicked() {
        ShiShi.instance.removeAllMakeup()
        var success = true
        
        for i in (0 ..< triplets.count) {
            
            if let triplet = triplets[i] {
                ShiShi.instance.putTripletInCurrentLook(triplet, disableOthersInCategory:false)
            } else {
                success = false
            }
        }
        
        if success {
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LookTryOnGroupTableViewCell", for: indexPath) as! LookTryOnGroupTableViewCell
        
        let triplet = triplets[indexPath.row]
        
        cell.setProduct(triplet!)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 180
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) {
            return 187 * tableView.frame.width / 375
        } else if (section == 1) {
            return 90
        } else {
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0000001
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (section == 0) {
            return 0
        } else if (section == 1) {
            return triplets.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if (section == 0) {
            let  bannerHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BannerHeaderId")! as! BannerHeader
            bannerHeader.setImage(imageName: "rooftop-bronze")
            
            return bannerHeader
        } else if (section == 1){
            let  lookInfoHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LookInfoHeaderId") as! LookInfoHeader
            lookInfoHeader.lookInfoView.delegate = self
            return lookInfoHeader
        } else {
            return nil
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    private var triplets = Array<TripletOfLook?>()
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var tagLineLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var moreButton: UIButton!
    
    func backButtonClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func showDescription() {
        
    }
    
    func showTagline() {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = .white
        
        tableView.register(UINib(nibName: "BannerHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "BannerHeaderId")
        
        tableView.register(UINib(nibName: "LookInfoHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "LookInfoHeaderId")
        
        tableView.register(UINib(nibName: "SimpleTextTableViewCell", bundle: nil), forCellReuseIdentifier: "SimpleTextTableViewCellId")
        
        tableView.register(UINib(nibName: "LookTryOnGroupTableViewCell", bundle:nil), forCellReuseIdentifier: "LookTryOnGroupTableViewCell")
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
        
        
        if (ShiShi.instance.isInitialized()) {
            loadEverythingAboutLook()
        } else {
            let realm = try! Realm()
            ShiShi.instance.initialize(realm, baseUrl: "https://sg.shishiapp.com", completion: {
                self.loadEverythingAboutLook()
            })
        }
    }
    
    
    func loadEverythingAboutLook() {
        
        let jsonString = "{\"tryon_groups\":[{\"items\":[472,473,471],\"parameter\":0,\"style\":46,\"category\":2},{\"items\":[472],\"parameter\":50,\"style\":49,\"category\":2},{\"items\":[528],\"parameter\":0,\"style\":18,\"category\":5}]}"
        
        ShiShi.instance.getTripletsOfLook(jsonString: jsonString, success: { [weak self] (triplets, more) in
            if let strongself = self {
                strongself.triplets = triplets
                DispatchQueue.main.async(execute: {
                    strongself.tableView.reloadData()
                })
            }
        }) { (error) in
            
        }
        
    
    }
    
    
    
    func closeButtonClicked() {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

