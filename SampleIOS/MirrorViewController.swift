//
//  MirrorViewController.swift
//  SampleIOS
//
//  Created by Lingyu Wang on 2018-05-13.
//  Copyright © 2018 ShiShi. All rights reserved.
//

import UIKit
import ShiShiSdk
import RealmSwift

class MirrorViewController: UIViewController {
    
    
    @IBOutlet weak var recordButtonContainer: UIView!
    
    @IBOutlet weak var beautifyButton: UIButton!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var productsButton: UIButton!
    @IBOutlet weak var cameraFlipButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var lookButton: UIButton!
    private var makeupViewController:MakeupViewController?
    
    private var recordButton : RecordButton?
    private var progressTimer : Timer!
    private var progress : CGFloat! = 0
    
    private var movieURL: URL?
    
    private let videoMaxDuration = 5; // seconds
    
    private var beautifyEnabled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeupViewController = MakeupViewController()
        makeupViewController?.view.frame = containerView.bounds
        
        containerView.addSubview((makeupViewController?.view)!)
        
        addChild(makeupViewController!)
        makeupViewController?.didMove(toParent: self)
        
        let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true, objectTypes: ShiShi.REALM_OBJECT_TYPES)
        let realm = try! Realm(configuration: config)
        ShiShi.instance.initialize(realm, baseUrl: "https://sg.shishiapp.com")
        
        beautifyEnabled = true
        ShiShi.instance.morphLevel = 3
        ShiShi.instance.smoothLevel = 3
        updateBeautifyButton()
        
        
        recordButton = RecordButton(frame: recordButtonContainer.bounds)
        recordButtonContainer.addSubview(recordButton!)
        
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        recordButton!.addGestureRecognizer(tapGestureRecognizer)
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        recordButton!.addGestureRecognizer(longPressRecognizer)
    }
    
    @objc func tapped(sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ImageCaptureViewController") as! ImageCaptureViewController
        vc.image = makeupViewController?.captureImage()
        present(vc, animated: false, completion: nil)
    }
    
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        
 
    }
    
    @objc func updateProgress() {
        
        let maxDuration = CGFloat(videoMaxDuration) // Max duration of the recordButton
        
        progress = progress + (CGFloat(0.05) / maxDuration)
        recordButton!.setProgress(progress)
        
        if progress >= 1 {
            progress = 0
            progressTimer.invalidate()
            recordButtonReleased()
            recordButton!.stopRecording()
        }
    }
    
    func recordButtonPressed() {

    }
    
    func recordButtonReleased() {

    }
    
    func recordButtonCancelled() {
        makeupViewController?.cancelRecording()
    }
    
    
    @IBAction func productsButtonClicked(_ sender: Any) {
    }
    
    @IBAction func beautifyButtonClicked(_ sender: Any) {
        beautifyEnabled = !beautifyEnabled
        if (beautifyEnabled) {
            ShiShi.instance.smoothLevel = 3
            ShiShi.instance.morphLevel = 3
        } else {
            ShiShi.instance.smoothLevel = 1
            ShiShi.instance.morphLevel = 0
        }
        updateBeautifyButton()
        ShiShi.instance.syncBeautify(makeupViewController)
    }
    
    private func updateBeautifyButton() {
        if (beautifyEnabled) {
            beautifyButton.alpha = 1.0
        } else {
            beautifyButton.alpha = 0.5
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        syncFeatures()
    }
    
    func syncFeatures() {
        ShiShi.instance.syncBeautify(makeupViewController)
        ShiShi.instance.syncAllMakeup(makeupViewController)
    }
    
    @IBAction func cameraFlipButtonClicked(_ sender: Any) {
        cameraFlipButton.isEnabled = false
        makeupViewController?.flipCamera({
            self.cameraFlipButton.isEnabled = true
        })
    }
    
    @IBAction func handlePan(recognizer:UIPanGestureRecognizer) {
        
        if (recognizer.state == .began) {
            ShiShi.instance.panCompareBegin(makeupViewController)
        } else if (recognizer.state == .ended) {
            ShiShi.instance.panCompareEnd(makeupViewController)
        }
        
        let location = recognizer.location(in: self.view)
        
        let position = Float(location.x * UIScreen.main.scale)
        
        ShiShi.instance.panComparePosition(makeupViewController, position: position)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

