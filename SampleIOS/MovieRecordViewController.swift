//
//  ViewController.swift
//  shishi-ios
//
//  Created by Lingyu Wang on 2017-05-10.
//  Copyright © 2017 ShiShi. All rights reserved.
//

import UIKit
import RealmSwift
import ShiShiSdk
import AVFoundation

class MovieRecordViewController: UIViewController {
    
    var movieURL:URL?
    var look:Look?
    
    
    @IBOutlet weak var movieView: UIView!
    @IBAction func closeButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBOutlet weak var closeButton: UIButton!
    
    var avPlayer: AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (movieURL != nil) {
            avPlayer = AVPlayer(url: movieURL!)
        }
        
        let videoLayer = AVPlayerLayer(player: avPlayer)
        videoLayer.frame = self.view.bounds
        videoLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        movieView.layer.addSublayer(videoLayer)
        
        avPlayer?.play()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.avPlayer?.currentItem, queue: .main) { _ in
            self.avPlayer?.seek(to: CMTime.zero)
            self.avPlayer?.play()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        avPlayer?.pause()
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
}

