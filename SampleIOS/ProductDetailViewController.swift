//
//  ProductDetailViewController.swift
//  SampleIOS
//
//  Created by Lingyu Wang on 2018-05-13.
//  Copyright © 2018 ShiShi. All rights reserved.
//

import UIKit
import RealmSwift
import AlamofireImage
import ShiShiSdk

class ProductDetailViewController: UIViewController, ColorSelectionViewProtocol, UITableViewDataSource, UITableViewDelegate {
    
    
    var fromMirror = false
    var colorSelectionHeader: ColorSelectionHeader?
    var bannerHeader: BannerHeader?
    
    private var selectedProductItemId:Int?
    private var selectedTryOnGroup:TryOnGroupOfLook?
    private var selectedIndex = 0
    private var needToScroll = false
    
    private var triplets = Array<Triplet>()
    private var concept: Concept?
    private var productItem: ProductItem?
    
    // Test data:
    
    // A Lipstick concept (Single try-on)
//    private var isConcept = true
//    private var contentIds = [110]
    
    // Multiple Lipstick product items (Single try-on)
//        private var isConcept = false
//        private var contentIds = [3, 5, 12]
    
    // A Lipstick product item (Single try-on)
//        private var isConcept = false
//        private var contentIds = [12]
    
    // A Eyeshadow product item (Non Single try-on)
//        private var isConcept = false
//        private var contentIds = [660]
    
    // Multiple Eyeshadow product items (Non Single try-on)
    private var isConcept = false
    private var contentIds = [275, 276, 277]
    
    private var texts = [String]()
    
    @IBOutlet weak var productTitleView: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    func colorSwatchSelected(index:Int) {
        selectedIndex = index
        doSelectItem()
    }
    
    func tryOnButtonClicked() {
        let selectedTriplet = triplets[selectedIndex]
        
        let selectedTripletOfLook = ShiShi.instance.createTripletOfLook(selectedTriplet)
        ShiShi.instance.removeAllTripletsFromCategory(selectedTriplet.0.category_id)
        ShiShi.instance.putTripletInCurrentLook(selectedTripletOfLook, disableOthersInCategory:true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func doSelectItem () {
        
        if (triplets.count == 0) {
            return
        }
        
        productItem = triplets[selectedIndex].1
        
        let offset = tableView.contentOffset
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.setContentOffset(offset, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = .white
        
        tableView.register(UINib(nibName: "BannerHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "BannerHeaderId")
        
        tableView.register(UINib(nibName: "ColorSelectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "ColorSelectionHeaderId")
        
        tableView.register(UINib(nibName: "SimpleTryOnHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "SimpleTryOnHeaderId")
        
        tableView.register(UINib(nibName: "SimpleTextTableViewCell", bundle: nil), forCellReuseIdentifier: "SimpleTextTableViewCellId")
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
        
        
        if (isConcept) {
            // Show all product items of the concept
            ShiShi.instance.getConcept(contentIds[0], success: { [weak self] (concept, more) in
                if let strongself = self {
                    strongself.concept = concept
                    strongself.productTitleView.text = concept.name
                    
                    let category = ShiShi.instance.getCategory(concept.category_ids[0].value)!
                    
                    ShiShi.instance.getTripletsOfConcept(concept, category: category, success: {[weak self] (triplets, more) in
                        
                        if let strongself = self {
                            strongself.triplets = triplets
                            strongself.locateAndSelectItem()
                            
                        }
                    }) { (error) in }
                    
                }
            }) { (error) in }
            
        } else {
            selectedProductItemId = contentIds[0]
            
            // Show single product item. If it's single tryon, also show its sibling product items from the same concept
            if (contentIds.count == 1) {
                
                ShiShi.instance.getProductItem(selectedProductItemId!, success: { [weak self] (productItem, more) in
                    if let strongself = self {
                        strongself.productItem = productItem
                        strongself.productTitleView.text = productItem.name
                        
                        ShiShi.instance.getConceptOfProductItem(strongself.selectedProductItemId!, success: { [weak self] (concept, more) in
                            if let strongself = self {
                                strongself.concept = concept
                                
                                let category = ShiShi.instance.getCategory(concept.category_ids[0].value)!
                                
                                if (concept.single_tryon) {
//
                                    ShiShi.instance.getTripletsOfConcept(concept, category: category, success: { [weak self] (triplets, more) in
                                        if let strongself = self {
                                            strongself.triplets = triplets
                                            strongself.locateAndSelectItem()
                                        }
                                    }) { (error) in }
                                    
                                    
                                } else {
                                    if let productItem = strongself.productItem {
                                        
                                        strongself.triplets = ShiShi.instance.loadTripletsOfProductItem(productItem, concept:concept, category:category)
                                        
                                        strongself.locateAndSelectItem()
                                    }
                                }
                            }
                        }) { (error) in }
                    }
                }) { (error) in }
                
            } else {
                // Show multiple product items of a concept
                
                ShiShi.instance.getTripletsOfProductItems(contentIds, success: { [weak self] (triplets, more) in
                    if let strongself = self {
                        if (triplets.count > 0) {
                            strongself.concept = triplets[0].2
                            strongself.productTitleView.text = triplets[0].2.name
                            strongself.triplets = triplets
                            strongself.locateAndSelectItem()
                        }
                    }
                }) { (error) in }
                
            }
        }
        
    }
    
    
    func locateAndSelectItem() {
        
        selectedIndex = 0
        
        if (concept?.single_tryon)! {
            
            if (selectedProductItemId != nil) {
                for i in 0..<triplets.count {
                    if triplets[i].1.pk == selectedProductItemId {
                        selectedIndex = i
                        break
                    }
                }
            }
        } else {
            if (selectedTryOnGroup != nil) {
                for i in 0..<triplets.count {
                    let tryOnGroup = triplets[i].0
                    
                    if tryOnGroup.equals(selectedTryOnGroup!) {
                        selectedIndex = i
                        break
                    }
                }
            }
        }
        
        needToScroll = true
        
        doSelectItem()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if (section == 0) {
            bannerHeader = (tableView.dequeueReusableHeaderFooterView(withIdentifier: "BannerHeaderId") as! BannerHeader)
            
            if (triplets.count > 0) {
                let selectedProductItem = triplets[selectedIndex].1
                bannerHeader?.setImageFitURL(imageUrl: ShiShiUrl.getImageUrl(selectedProductItem.image).absoluteString)
            } else {
                bannerHeader?.setImageFitURL(imageUrl: "")
            }
            
            return bannerHeader
        }  else if (section == 1) {
            
            colorSelectionHeader = (tableView.dequeueReusableHeaderFooterView(withIdentifier: "ColorSelectionHeaderId") as! ColorSelectionHeader)
            colorSelectionHeader?.colorSelectionView.delegate = self
            colorSelectionHeader!.colorSelectionView.setTriplets(triplets)
            
            colorSelectionHeader!.colorSelectionView.selectedIndex = selectedIndex
            
            colorSelectionHeader!.colorSelectionView.reloadData()
            if (needToScroll) {
                colorSelectionHeader!.colorSelectionView.scrollToCurrentSelection()
                needToScroll = false
            }
            
            return colorSelectionHeader
            
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) {
            return 187 * tableView.frame.width / 375
        } else if (section == 1) {
            return 140
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0000001
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return 0
        } else if (section == 1) {
            
            if (triplets.count == 0) {
                return 0
            }
            
            texts.removeAll()
            var i = 0
            
            if !(concept?.tag_line.isEmpty)! {
                texts.append((concept?.tag_line)!)
                i = i + 1
            }
            
            if (concept?.single_tryon)! {
                let productItem = triplets[selectedIndex].1
                if !(productItem.name.isEmpty || productItem.name == concept?.name) {
                    
                    texts.append(productItem.name)
                    i = i + 1
                }
                
                let tryOnItem = triplets[selectedIndex].0.tryon_items[0]
                if !(tryOnItem.getColorFinishString().isEmpty) {
                    
                    texts.append(tryOnItem.getColorFinishString())
                    i = i + 1
                }
                
            } else {
                let tryOnGroup = triplets[selectedIndex].0
                if (tryOnGroup.tryon_items.count == 1) {
                    let tryOnItem = tryOnGroup.tryon_items[0]
                    if !(tryOnItem.getColorFinishString().isEmpty) {
                        
                        texts.append(tryOnItem.getColorFinishString())
                        i = i + 1
                    }
                }
            }
            
            if !(concept?.concept_description.isEmpty)! {
                texts.append((concept?.concept_description)!)
                i = i + 1
            }
            
            if !(concept?.ingredient_effect.isEmpty)! {
                texts.append((concept?.ingredient_effect)!)
                i = i + 1
            }
            
            if !(concept?.howto.isEmpty)! {
                texts.append((concept?.howto)!)
                i = i + 1
            }
            
            if !(productItem?.product_item_description.isEmpty)! {
                texts.append((productItem?.product_item_description)!)
                i = i + 1
            }
            
            
            return i
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SimpleTextTableViewCellId", for: indexPath) as! SimpleTextTableViewCell
        
        if (triplets.count == 0) {
            return UITableViewCell()
        }
        
        cell.setText(texts[indexPath.row], lines: 100)
        
        cell.separatorInset = UIEdgeInsets(top: 0, left: cell.bounds.size.width, bottom: 0, right: 0)
        
        return cell
    }
    
    
}

