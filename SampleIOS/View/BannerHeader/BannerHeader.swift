//
//  BannerHeader.swift
//  SampleIOS
//
//  Created by Lingyu Wang on 2018-05-13.
//  Copyright © 2018 ShiShi. All rights reserved.
//

import UIKit
import ShiShiSdk

class BannerHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var bannerImage: UIImageView!
    
    override var contentView:UIView {
        
        get {
            return self.subviews[0]
        }
    }
    
    func setImageFitURL(imageUrl: String) {
        bannerImage.contentMode = UIView.ContentMode.scaleAspectFit
        bannerImage.af_setImage(withURL: ShiShiUrl.getImageUrl(imageUrl))
    }
    
    func setImageURL(imageUrl: String) {
        bannerImage.contentMode = UIView.ContentMode.scaleAspectFill
        bannerImage.af_setImage(withURL: ShiShiUrl.getImageUrl(imageUrl), placeholderImage: UIImage(named:"placeholder"))
    }
    
    func setImage(imageName: String) {
        bannerImage.contentMode = UIView.ContentMode.scaleAspectFill
        bannerImage.image = UIImage(named: imageName)
    }

}
