//
//  ColorSelectionHeader.swift
//  SampleIOS
//
//  Created by Lingyu Wang on 2018-05-13.
//  Copyright © 2018 ShiShi. All rights reserved.
//

import UIKit


class ColorSelectionHeader: UITableViewHeaderFooterView {
    

    @IBOutlet weak var colorSelectionView: ColorSelectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
