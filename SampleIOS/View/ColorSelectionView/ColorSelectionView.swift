//
//  ColorSelectionViewProtocol.swift
//  SampleIOS
//
//  Created by Lingyu Wang on 2018-05-13.
//  Copyright © 2018 ShiShi. All rights reserved.
//

import UIKit
import ShiShiSdk

protocol ColorSelectionViewProtocol : class {
    
    func tryOnButtonClicked()
    func colorSwatchSelected(index:Int)
    
}


@IBDesignable class ColorSelectionView: UIView, UICollectionViewDataSource, UICollectionViewDelegate {
    
    weak var view: UIView!

    
    @IBOutlet weak var tryOnButton: UIButton!
    var selectedIndex = 0
    
    private var triplets = Array<Triplet>()
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBAction func tryOnButtonClicked(_ sender: Any) {
        delegate?.tryOnButtonClicked()
    }
    
    public weak var delegate:ColorSelectionViewProtocol?
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return triplets.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        collectionView.reloadData()
        
        delegate?.colorSwatchSelected(index:indexPath.row)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorSwatchCollectionViewCell", for: indexPath) as! ColorSwatchCollectionViewCell
        
        let tryOnGroup = triplets[indexPath.row].0
        
        cell.setTryOnItems(tryOnGroup.tryon_items)
        
        
        if (indexPath.row == selectedIndex) {
            cell.layer.borderWidth = 1.0
            cell.layer.borderColor = UIColor.black.cgColor
        } else {
            cell.layer.borderWidth = 0.0
            
        }
        
        return cell
    }

    
    
    
    override init(frame: CGRect) {

        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)
        
        xibSetup()
    }

    
    @IBOutlet weak var shareButton: UIButton!
    
    func xibSetup() {
        view = loadViewFromNib()
        
        view.frame = bounds
        
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]

        addSubview(view)
        
        
        //16+21 check out the constraints in storyboard
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 36, bottom: 0, right: 36)
        
        collectionView.register(ColorSwatchCollectionViewCell.self, forCellWithReuseIdentifier: "ColorSwatchCollectionViewCell")
        
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ColorSelectionView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    func setTriplets(_ triplets:[Triplet]) {
        self.triplets = triplets
    }
    

    func reloadData() {
        collectionView.reloadData()
    }
    
    func scrollToCurrentSelection() {
        if (selectedIndex < triplets.count) {
            collectionView.scrollToItem(at: IndexPath(row: selectedIndex, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: false)
        }
    }
    
    
    
    

}
