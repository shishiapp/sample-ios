//
//  MainHeaderViewCell.swift
//  shishi-ios
//
//  Created by Lingyu Wang on 2017-05-11.
//  Copyright © 2017 ShiShi. All rights reserved.
//

import UIKit


class LookInfoHeader: UITableViewHeaderFooterView {
    

    @IBOutlet weak var lookInfoView: LookInfoView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
