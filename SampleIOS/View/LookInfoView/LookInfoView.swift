//
//  LookInfoView.swift
//  shishi-ios
//
//  Created by Lingyu Wang on 2017-05-14.
//  Copyright © 2017 ShiShi. All rights reserved.
//

import UIKit
import ShiShiSdk

protocol LookInfoViewProtocol : class {
    
    func tryOnButtonClicked()
}


@IBDesignable class LookInfoView: UIView {
    
    weak var view: UIView!
    
    var buttons = [UIButton]()
    
    @IBAction func tryOnButtonClicked(_ sender: Any) {
        delegate?.tryOnButtonClicked()
    }
    
    public weak var delegate:LookInfoViewProtocol?
    
    
    
    override init(frame: CGRect) {

        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)
        
        xibSetup()
    }

    func xibSetup() {
        view = loadViewFromNib()
        
        view.frame = bounds
        
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "LookInfoView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
}
