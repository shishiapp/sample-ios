//
//  ViewController.swift
//  shishi-ios
//
//  Created by Lingyu Wang on 2017-05-10.
//  Copyright © 2017 ShiShi. All rights reserved.
//

import UIKit
import ShiShiSdk


class LookTryOnGroupTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var styleImageView: UIImageView!
    @IBOutlet weak var brandConceptLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    
    @IBOutlet weak var colorSwatchView: ColorSwatchView!
    
    @IBOutlet weak var productItemImageView: UIImageView!
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    
    
    var triplet: TripletOfLook?
    
    var index = 0
    
    func setIndex(index:Int) {
        self.index = index
    }
    
    func setProduct(_ triplet: TripletOfLook) {
        
        
        self.triplet = triplet
        
        let tryOnGroup = triplet.0
        let productItem = triplet.1
        let concept = triplet.2
        
        //        categoryLabel.text = "- " + (Singleton.shared.categoryDictionary[tryOnGroup.category_id]?.name)! + " -"
        if let category = ShiShi.instance.getCategory(tryOnGroup.category_id) {
            categoryLabel.text = category.name
        }
        
        productNameLabel.text = concept.name
        
        brandConceptLabel.text = "Brand" // basic use of the SDK, can't get Brand name. Use client API to get Brand info instead.
        
        
        colorSwatchView.setTryOnItems(tryOnGroup.tryon_items)
        
        productItemImageView?.af_setImage(withURL: ShiShiUrl.getImageUrl(productItem.image))
        
        let style = tryOnGroup.style!
        
        ShiShi.instance.setAssetImage(imageView: styleImageView, image: style.image)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if(selected) {
            colorSwatchView.setTryOnItems(triplet?.0.tryon_items)
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        if(highlighted) {
            colorSwatchView.setTryOnItems(triplet?.0.tryon_items)
        }
    }
    
    
    
    
    
}

