//
//  SimpleTextTableViewCell.swift
//  SampleIOS
//
//  Created by Lingyu Wang on 2018-05-13.
//  Copyright © 2018 ShiShi. All rights reserved.
//

import UIKit

class SimpleTextTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var simpleTextLabel: UILabel!
    
    func setText(_ text:String, lines:Int) {
        
        
        leadingConstraint.constant = 60
        trailingConstraint.constant = 60
        
        simpleTextLabel.text = text
        simpleTextLabel.numberOfLines = lines
        
        
        simpleTextLabel.sizeToFit()
        simpleTextLabel.layoutIfNeeded()
    }

}

